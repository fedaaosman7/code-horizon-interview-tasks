// Created components
import Countries from "./components/countries/Countries";

// CSS files
import 'bootstrap/dist/css/bootstrap.min.css';
import './Task2.css';

/**
 *
 * @returns {JSX.Element}
 */
function Task2() {
    return (
        <div className='custom-task-2'>
            {/*  !* Complete the code in the inner sections to make this work!  */}
            <Countries />
        </div>
    );
}

export default Task2;