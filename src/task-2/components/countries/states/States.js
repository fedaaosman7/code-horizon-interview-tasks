// Created components
// !* un-comment the component below to make use of it in data display.
// import State from "./state/State";

// CSS files
import './States.css';

/**
 *
 * @returns {JSX.Element}
 */
function States({ states=[] }) {

    // !* Here you need to iterate over each state and display its info, make use of the State component.

    return (
        <ul className='states'>

        </ul>
    );
}

export default States;