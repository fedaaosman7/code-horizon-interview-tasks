// React hooks and functionality
import { useState } from "react";

// SVGs
import { ReactComponent as EditIcon } from "../../../../assets/edit-icon.svg";
import { ReactComponent as DeleteIcon } from "../../../../assets/delete-icon.svg";

// CSS files
import './State.css';

/**
 *
 * @returns {JSX.Element}
 */
function State({ name='' }) {
    const [showOptions, setShowOptions] = useState(false);

    // !* Complete the logic to add edit and delete functionality to each item.

    return (
        <li className='state'>
            <h5 onClick={() => setShowOptions(!showOptions)}>{ name }</h5>
            {
                showOptions ?
                    <div className='state-icons-container'>
                        <EditIcon className='edit' />
                        <DeleteIcon className='delete' />
                    </div>
                    :
                    null
            }
        </li>
    );
}

export default State;