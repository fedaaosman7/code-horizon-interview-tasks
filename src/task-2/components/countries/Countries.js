// Created components
// !* Make use of this component to display each single piece of data.
// import States from "./states/States";

// CSS files
import './Countries.css';

/**
 *
 * @returns {JSX.Element}
 */
function Countries({ countries=[] }) {

    // !* Complete the code to display the items you've fetched from the backend.
    // !* You must display the country name here as a h3 tag, and for each country name there are states to be displayed

    return (
        <div className='countries'>
            <ul>

            </ul>
        </div>
    );
}

export default Countries;