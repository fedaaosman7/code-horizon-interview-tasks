// React Bootstrap components
import Spinner from "react-bootstrap/Spinner";

// CSS files
import './CustomButtonSpinner.css';

/**
 * A spinner that is rendered next to the text in a button, represents loading functionality.
 *
 * @param   {String}      className     The extra class/classes to use to style the element
 * @param   {String}      size          The size of the spinner, to be passed down to React's Spinner component
 * @returns {JSX.Element}
 */
function CustomButtonSpinner({ className='', size='sm' }) {
    return (
        <Spinner className={`custom-button-spinner ${className}`} size={size}/>
    );
}

export default CustomButtonSpinner;