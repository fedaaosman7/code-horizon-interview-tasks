// React hooks and functionality
import { BrowserRouter as Router } from "react-router-dom";

// Created components
import SiteRoutes from "./routes/SiteRoutes";

// CSS files
import './App.css';

function App() {
  return (
      <Router>
        <div className="App">
            <SiteRoutes />
        </div>
      </Router>
  );
}

export default App;
